<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 10:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="partials/header.jsp" %>

<div class="container-custom mt-5">
    <h1 class="title">Se connecter</h1>
    <form action="login" class="container" method="post">
        <div class="form-group">
            <label for="inputEmail">Email</label>
            <input type="email" class="form-control" name="inputEmail" id="inputEmail">
        </div>
        <div class="form-group">
            <label for="inputPassword">Mot de passe</label>
            <input type="password" class="form-control" name="inputPassword" id="inputPassword">
        </div>

        <button type="submit" class="btn btn-custom button-login">Se connecter</button>
    </form>
</div>

<%@include file="partials/footer.jsp" %>
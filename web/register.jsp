<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="partials/header.jsp" %>

<div class="container-custom mt-5">
    <h1 class="title">S'inscrire</h1>
    <form action="register" method="post">
        <div class="form-group">
            <label for="inputEmail">Email</label>
            <input type="email" class="form-control" name="inputEmail" id="inputEmail">
        </div>
        <div class="form-group">
            <label for="inputLastName">Nom</label>
            <input type="text" class="form-control" name="inputLastName" id="inputLastName">
        </div>
        <div class="form-group">
            <label for="inputFirstName">Prénom</label>
            <input type="text" class="form-control" name="inputFirstName" id="inputFirstName">
        </div>
        <div class="form-group">
            <label for="inputPassword">Mot de passe</label>
            <input type="password" class="form-control" name="inputPassword" id="inputPassword">
        </div>
        <div class="form-group">
            <label for="inputConfirmPassword">Confirmer le mot de passe</label>
            <input type="password" class="form-control" name="inputConfirmPassword" id="inputConfirmPassword">
        </div>

        <button type="submit" class="btn btn-custom button-register">S'inscrire</button>
    </form>
</div>

<%@include file="partials/footer.jsp" %>
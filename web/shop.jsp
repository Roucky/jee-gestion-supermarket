<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="partials/header.jsp" %>

<div class="container">
    <h1 class="title">Boutique</h1>
    <div class="shop">
        <c:forEach items="${ articles.values() }" var="article">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><c:out value="${ article.libelle }"></c:out></h5>
                    <img src="${ article.urlImg }" class="card-img-top" alt="Image ${ article.libelle }">
                    <p class="card-text">Code barre : ${ article.codeBarre }</p>
                </div>
                <div class="card-footer d-flex align-items-center">
                    <p class="card-text-price mb-0 mr-2"><fmt:formatNumber value="${ article.prixTTCEuro }" type="currency"/></p>
                    <form action="cart" class="mx-auto mb-0" method="post">
                        <button name="inputCodeBarre" value="${ article.codeBarre }" class="btn btn-custom"
                                type="submit">Ajouter au panier
                        </button>
                    </form>
                </div>
            </div>
        </c:forEach>
    </div>
</div>


<%@ include file="partials/footer.jsp" %>
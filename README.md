**Compte-rendu TP Supermarché**

__Equipe__ :
-LEQUETTE Corentin
-CSERNAK Fabien

URL du dépôt GitLab et informations sur l’application :
https://gitlab.com/Roucky/jee-gestion-supermarket
Compte administrateur se créant au lancement du serveur :

__Email__ : admin@admin.fr

__Mot de passe__ : admin

Des données de test sont aussi créées au lancement du serveur.

__Organisation du temps__ :
Nous avons travaillé sur le projet durant les deux jours de cours + le soir du premier jour grâce au partage d’écran sur Discord.

__Organisation du travail__ :
Pour se répartir les tâches, nous avons utilisé le système de branches de GitLab afin de pouvoir développer les différentes fonctionnalités de l’application et de pouvoir se partager le code sans problème.
Nous avons travaillé la plupart du temps à deux et développé les fonctionnalités ensemble.
Le premier jour (cours + soir), nous avons développé les fonctionnalités principales demandées (gestion de la base des articles, gestion du ticket de caisse et authentification)
Le deuxième jour, nous avons amélioré ces fonctionnalités (résolution de bugs, etc), réalisé le design de l’application et ajouter quelques fonctionnalités supplémentaires.
 
__Travail accompli__ :
Notre application contient les besoins demandés dans l’énoncé du cas pratique :
-Gestion de la base des articles
-Gestion du ticket de caisse (panier)
-Authentification

Pour gérer les articles, nous les avons stockés dans le contexte de l’application, sous la forme d’une Map ayant pour clé le code barre et pour valeur l’article. (comme prévu dans l’énoncé)
Pour les utilisateurs, nous les avons stocké dans le contexte d’application, sous la forme d’une Map ayant pour clé l’email et pour valeur l’utilisateur (Classe)
Enfin, pour le ticket de caisse, nous avons stocké les lignes dans la session (pour correspondre à l’utilisateur actuel), dans une Map ayant pour clé le code barre du produit (et non pas le produit en cas de modification de celui-ci par un administrateur) et pour valeur la quantité souhaitée.

Fonctionnalités supplémentaires :
-Ajout d’un article au panier grâce à un bouton sur la page « boutique »
-Gestion de la quantité d’un article dans le panier avec possibilité de la diminuer ou l’augmenter
-Un article possède une image via une URL (nous voulions faire l’ajout d’image via l’upload mais par manque de temps nous n’avons pas réalisé cette fonctionnalité)
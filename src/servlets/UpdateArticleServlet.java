package servlets;

import classes.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/admin/articles/update")
public class UpdateArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean error = false;
        long actualCodeBarre = 0;
        long codeBarre = 0;
        float prixHT = 0;
        int tauxTVA = 0;
        try {
            actualCodeBarre = Long.parseLong(request.getParameter("codeBarre"));
            codeBarre = Long.parseLong(request.getParameter("inputCodeBarre"));
            prixHT = Float.parseFloat(request.getParameter("inputPrixHT"));
            tauxTVA = Integer.parseInt(request.getParameter("inputTauxTVA"));
        } catch (Exception e) {
            error = true;
            request.setAttribute("error", "Une erreur est survenue !");
        }

        String reference = request.getParameter("inputReference");
        String libelle = request.getParameter("inputLibelle");
        String urlImg = request.getParameter("inputUrlImg");

        if (reference != null && libelle != null && urlImg != null && error == false) {
            Article article = new Article(codeBarre, reference, libelle, (int) (prixHT * 100), Article.tableauTauxTVA[tauxTVA], urlImg);

            ServletContext context = this.getServletContext();

            Map<Long, Article> articles = (HashMap<Long, Article>) context.getAttribute("articles");

            // Suppression avant ajout pour éviter les doublons si l'on change de code barre

            articles.remove(actualCodeBarre);
            articles.put(codeBarre, article);

            context.setAttribute("articles", articles);

            response.sendRedirect(request.getContextPath() + "/admin/articles");
            return;
        } else {
            request.setAttribute("error", "Une erreur est survenue !");
        }

        request.setAttribute("tauxTVAs", Article.tableauTauxTVA);
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/admin/article_add.jsp");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long codeBarre = Long.parseLong(request.getParameter("codeBarre"));

        ServletContext context = this.getServletContext();

        Map<Long, Article> articles = (HashMap<Long, Article>) context.getAttribute("articles");

        if (articles.containsKey(codeBarre)) {
            request.setAttribute("article", articles.get(codeBarre));
            request.setAttribute("tauxTVAs", Article.tableauTauxTVA);
            RequestDispatcher dispatcher = context.getRequestDispatcher("/admin/article_update.jsp");
            dispatcher.forward(request, response);
            return;
        }

        request.setAttribute("error", "Cet article n'existe pas !");
        response.sendRedirect(request.getContextPath() + "/admin/articles");
    }
}

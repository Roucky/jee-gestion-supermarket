package servlets;

import classes.Utilisateur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("inputEmail");
        String lastName = request.getParameter("inputLastName");
        String firstName = request.getParameter("inputFirstName");
        String password = request.getParameter("inputPassword");
        String confirmPassword = request.getParameter("inputConfirmPassword");

        if (password.equals(confirmPassword)) {
            Utilisateur utilisateur = new Utilisateur(email, lastName, firstName, password, Utilisateur.roles[0]);

            Map<String, Utilisateur> utilisateurs;

            ServletContext context = this.getServletContext();

            if (context.getAttribute("utilsateurs") != null) {
                utilisateurs = (HashMap<String, Utilisateur>) context.getAttribute("utilisateurs");
            } else {
                utilisateurs = new HashMap<String, Utilisateur>();
            }

            utilisateurs.put(email, utilisateur);
            context.setAttribute("utilisateurs", utilisateurs);

            HttpSession session = request.getSession(true);

            session.setAttribute("authenticated", true);
            session.setAttribute("email", utilisateur.getEmail());
            session.setAttribute("role", utilisateur.getRole());

            response.sendRedirect(request.getContextPath() + "/");
            return;
        }

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/register.jsp");
        request.setAttribute("error", "Les mots de passes sont différents");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/register.jsp");
        dispatcher.forward(request, response);
    }
}

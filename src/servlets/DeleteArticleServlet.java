package servlets;

import classes.Article;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/admin/articles/delete")
public class DeleteArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long codeBarre = Long.parseLong(request.getParameter("codeBarre"));

        ServletContext context = this.getServletContext();

        Map<Long, Article> articles = (HashMap<Long, Article>) context.getAttribute("articles");

        articles.remove(codeBarre);

        context.setAttribute("articles", articles);

        response.sendRedirect(request.getContextPath() + "/admin/articles");
    }
}

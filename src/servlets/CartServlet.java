package servlets;

import classes.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionCookieConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@WebServlet("/cart")
public class CartServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean error = false;
        long codeBarre = 0;
        try {
            codeBarre = Long.parseLong(request.getParameter("inputCodeBarre"));
        } catch (Exception e) {
            error = true;
        }
        String action = request.getParameter("action");

        if (action == null) {
            action = "plus";
        }

        ServletContext context = this.getServletContext();

        Map<Long, Article> articles = (HashMap<Long, Article>) context.getAttribute("articles");

        if (error == false && articles.containsKey(codeBarre)) {
            Article article = articles.get(codeBarre);

            HttpSession session = request.getSession(true);

            Map<Long, Integer> cart;

            if (session.getAttribute("cart") != null) {
                cart = (HashMap<Long, Integer>) session.getAttribute("cart");
            } else {
                cart = new HashMap<>();
            }

            if (cart.containsKey(codeBarre)) {
                int quantite = cart.get(codeBarre);
                if (action.equals("minus")) {
                    quantite--;
                } else {
                    quantite++;
                }

                if (quantite == 0) {
                    cart.remove(codeBarre);
                } else {
                    cart.put(codeBarre, quantite);
                }
            } else {
                if (action.equals("plus")) {
                    cart.put(codeBarre, 1);
                }
            }

            session.setAttribute("cart", cart);

            response.sendRedirect(request.getContextPath() + "/cart");
            return;
        }

        request.setAttribute("error", "Cet article n'existe pas !");
        RequestDispatcher dispatcher = context.getRequestDispatcher("/cart.jsp");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/cart.jsp");
        dispatcher.forward(request, response);
    }
}

package servlets;

import classes.Utilisateur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("inputEmail");
        String password = request.getParameter("inputPassword");

        ServletContext context = this.getServletContext();

        Map<String, Utilisateur> utilisateurs;
        context.getAttribute("utilisateurs");
        if (context.getAttribute("utilisateurs") != null) {
            utilisateurs = (HashMap<String, Utilisateur>) context.getAttribute("utilisateurs");
            Utilisateur utilisateur = utilisateurs.get(email);

            if (utilisateur != null) {
                if (password.equals(utilisateur.getPassword())) {
                    HttpSession session = request.getSession(true);

                    session.setAttribute("authenticated", true);
                    session.setAttribute("email", utilisateur.getEmail());
                    session.setAttribute("role", utilisateur.getRole());

                    response.sendRedirect(request.getContextPath() + "/");
                    return;
                }
            }
        }
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
        request.setAttribute("error", "Email ou mot de passe incorrect !");
        dispatcher.forward(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        HttpSession session = request.getSession(false);
        if (session.getAttribute("authenticated") != null) {
            response.sendRedirect(request.getContextPath() + "/");
            return;
        }
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
    }
}

package classes;

public class Article {

    private long codeBarre;
    private String reference;
    private String libelle;
    private int prixHT;
    private int tauxTVA;
    private String urlImg;

    public final static int[] tableauTauxTVA = {550, 2000};

    public Article(long codeBarre, String reference, String libelle, int prixHT, int tauxTVA, String urlImg) {
        this.codeBarre = codeBarre;
        this.reference = reference;
        this.libelle = libelle;
        this.prixHT = prixHT;
        this.tauxTVA = tauxTVA;
        this.urlImg = urlImg;
    }

    public long getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(long codeBarre) {
        this.codeBarre = codeBarre;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(int prixHT) {
        this.prixHT = prixHT;
    }

    public int getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(int tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

//    Prix TTC
    public int getPrixTTC() {
        float prixTTC = prixHT * (1 + (float) tauxTVA / 10000);
        return Math.round(prixTTC);
    }

//    Les méthodes finissant par Euro renvoient la valeur en euro (float)
    public float getPrixHTEuro() {
        return (float) prixHT / 100;
    }

    public float getPrixTTCEuro() {
        return (float) getPrixTTC() / 100;
    }

    public float getTVAEuro() {
        return getPrixTTCEuro() - getPrixHTEuro();
    }
}

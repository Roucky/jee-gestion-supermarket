package listeners;

import classes.Article;
import classes.Utilisateur;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.HashMap;
import java.util.Map;

@WebListener()
public class InitDataListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    // Public constructor is required by servlet spec
    public InitDataListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {

        // Initialisation des données
        ServletContext context = sce.getServletContext();

        Map<String, Utilisateur> utilisateurs = new HashMap<>();

        Utilisateur utilisateur = new Utilisateur("admin@admin.fr", "Admin", "Admin", "admin", Utilisateur.roles[1]);

        utilisateurs.put(utilisateur.getEmail(), utilisateur);

        context.setAttribute("utilisateurs", utilisateurs);

        Map<Long, Article> articles = new HashMap<>();

        Article article = new Article(1000, "1", "Cereales", 283, Article.tableauTauxTVA[0], "https://bg-foods-medium.s3.amazonaws.com/cream-of-wheat-ca/products/COW%20Instant%20whole%20grain%20render%209-18%202.no%20health%20check%20v2.png");
        Article article2 = new Article(2000, "2", "Gel douche", 248, Article.tableauTauxTVA[0], "https://www.sport-nutrition-center.com/media/catalog/product/cache/aefcd4d8d5c59ba860378cf3cd2e94da/g/e/gel-douche-foucaud-web_1.png");
        Article article3 = new Article(3000, "3", "Lessive", 409, Article.tableauTauxTVA[1], "https://images-na.ssl-images-amazon.com/images/I/61zkgxeQ7sL._AC_SL1000_.jpg");
        Article article4 = new Article(4000, "4", "Pack d'eau", 105, Article.tableauTauxTVA[1], "https://www.carrefour.fr/media/540x540/Photosite/PGC/BOISSONS/3274080001005_PHOTOSITE_20191122_052152_0.jpg");
        Article article5 = new Article(5000, "5", "Sachet de pomme de terre", 500, Article.tableauTauxTVA[1], "https://img.plusdebonsplans.com/2018/02/pommes-de-terre-chez-auchan.jpg");

        articles.put(article.getCodeBarre(), article);
        articles.put(article2.getCodeBarre(), article2);
        articles.put(article3.getCodeBarre(), article3);
        articles.put(article4.getCodeBarre(), article4);
        articles.put(article5.getCodeBarre(), article5);

        context.setAttribute("articles", articles);
    }

    public void contextDestroyed(ServletContextEvent sce) {
      /* This method is invoked when the Servlet Context 
         (the Web application) is undeployed or 
         Application Server shuts down.
      */
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
        /* Session is created. */
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        /* Session is destroyed. */
    }

    // -------------------------------------------------------
    // HttpSessionAttributeListener implementation
    // -------------------------------------------------------

    public void attributeAdded(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute 
         is added to a session.
      */
    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {
      /* This method is called when an attribute
         is removed from a session.
      */
    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {
      /* This method is invoked when an attibute
         is replaced in a session.
      */
    }
}

<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 16:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../partials/header.jsp" %>

<div class="container-custom">
    <h1 class="title">Modification de l'article ${ article.libelle }</h1>
    <form method="post">
        <div class="form-group">
            <label for="inputCodeBarre">Code barre</label>
            <input type="text" class="form-control" name="inputCodeBarre" id="inputCodeBarre"
                   value="${ article.codeBarre }" required>
        </div>
        <div class="form-group">
            <label for="inputReference">Référence</label>
            <input type="text" class="form-control" name="inputReference" id="inputReference"
                   value="${ article.reference }" required>
        </div>
        <div class="form-group">
            <label for="inputLibelle">Libellé</label>
            <input type="text" class="form-control" name="inputLibelle" id="inputLibelle" value="${ article.libelle }"
                   required>
        </div>
        <div class="form-group">
            <label for="inputPrixHT">Prix Hors Taxes</label>
            <input type="text" class="form-control" name="inputPrixHT" id="inputPrixHT" value="${ article.prixHTEuro }"
                   required>
        </div>
        <div class="form-group">
            <label for="inputTauxTVA">Taux de TVA</label>
            <select class="form-control" name="inputTauxTVA" id="inputTauxTVA" required>
                <c:forEach items="${ tauxTVAs }" var="tauxTVA" varStatus="status">
                    <c:if test="${ tauxTVA == article.tauxTVA }">
                        <option value="${ status.index }" selected><fmt:formatNumber value="${ tauxTVA / 100 }"/> %
                        </option>
                    </c:if>
                    <c:if test="${ tauxTVA != article.tauxTVA }">
                        <option value="${ status.index }"><fmt:formatNumber value="${ tauxTVA / 100 }"/> %</option>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="inputUrlImg">URL de l'image</label>
            <input type="text" class="form-control" name="inputUrlImg" id="inputUrlImg" value="${ article.urlImg }"
                   required>
        </div>
        <button type="submit" class="btn btn-custom">Modifier</button>
    </form>
</div>

<%@ include file="../partials/footer.jsp" %>
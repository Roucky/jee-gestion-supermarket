<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../partials/header.jsp" %>
<div class="container-custom">
    <h1 class="title">Ajout d'un article</h1>
    <form method="post">
        <div class="form-group">
            <label for="inputCodeBarre">Code barre</label>
            <input type="text" class="form-control" name="inputCodeBarre" id="inputCodeBarre" required>
        </div>
        <div class="form-group">
            <label for="inputReference">Référence</label>
            <input type="text" class="form-control" name="inputReference" id="inputReference" required>
        </div>
        <div class="form-group">
            <label for="inputLibelle">Libellé</label>
            <input type="text" class="form-control" name="inputLibelle" id="inputLibelle" required>
        </div>
        <div class="form-group">
            <label for="inputPrixHT">Prix Hors Taxes</label>
            <input type="text" class="form-control" name="inputPrixHT" id="inputPrixHT" required>
        </div>
        <div class="form-group">
            <label for="inputTauxTVA">Taux de TVA</label>
            <select class="form-control" name="inputTauxTVA" id="inputTauxTVA" required>
                <c:forEach items="${ tauxTVAs }" var="tauxTVA" varStatus="status">
                    <option value="${ status.index }"><fmt:formatNumber value="${ tauxTVA / 100 }"/> %</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="inputUrlImg">URL de l'image</label>
            <input type="text" class="form-control" name="inputUrlImg" id="inputUrlImg" required>
        </div>
        <button type="submit" class="btn btn-custom">Ajouter</button>
    </form>
</div>

<%@ include file="../partials/footer.jsp" %>
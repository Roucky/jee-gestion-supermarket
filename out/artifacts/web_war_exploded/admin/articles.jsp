<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../partials/header.jsp" %>


<div class="container">
    <h1 class="title">Liste des articles</h1>
    <div class="shop">
        <c:forEach items="${ articles.values() }" var="article">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><c:out value="${ article.libelle }"></c:out></h5>
                    <img src="${ article.urlImg }" class="card-img-top" alt="Image ${ article.libelle }">
                    <p class="card-text"><fmt:formatNumber value="${ article.prixTTCEuro }" type="currency"/></p>
                    <p class="card-text">Code barre : ${ article.codeBarre }</p>
                    <p class="card-text">Reference : ${ article.reference }</p>
                    <p class="card-text">Taux TVA : <fmt:formatNumber value="${ article.tauxTVA / 100 }" pattern="0.00" type="percent" /> %</p>
                </div>
                <div class="card-footer d-flex align-items-center">
                    <form class="mx-auto mb-0" method="post">
                        <a href="<c:url value="/admin/articles/update" ><c:param name="codeBarre" value="${ article.codeBarre }" /></c:url>"
                           class="btn btn-custom">Modifier</a>
                        <a href="<c:url value="/admin/articles/delete" ><c:param name="codeBarre" value="${ article.codeBarre }" /></c:url>"
                           class="btn btn-custom">Supprimer</a>
                    </form>
                </div>
            </div>
        </c:forEach>
    </div>
    <a href="<c:url value="/admin/articles/add" />" class="float-right">Ajouter un article</a>
</div>


<%@ include file="../partials/footer.jsp" %>
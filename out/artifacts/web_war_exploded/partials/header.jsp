<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>My SuperMarket</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js">
    <link rel="stylesheet" href="<c:url value="${ contextPath }/css/style.css" />">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-custom">
    <div class="container">
        <a class="navbar-brand" href="<c:url value="/accueil" />">My SuperMarket</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<c:url value="/accueil" />">Accueil <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/shop" />">Boutique</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/cart" />"><i class="fas fa-cart-arrow-down"></i> Panier</a>
                </li>
                <c:if test="${ role == 'admin' }">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="adminDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Administrateur
                        </a>
                        <div class="dropdown-menu" aria-labelledby="adminDropdown">
                            <a class="dropdown-item" href="<c:url value="/admin/articles" />">Liste des articles</a>
                            <a class="dropdown-item" href="<c:url value="/admin/articles/add" />">Ajouter un article</a>
                        </div>
                    </li>
                </c:if>
            </ul>
            <ul class="navbar-nav">
                <c:if test="${ authenticated }">
                <span class="navbar-text">
                    Bonjour, ${ email }
                </span>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/logout"/>">Se déconnecter <i
                                class="fas fa-sign-out-alt"></i></a>
                    </li>
                </c:if>
                <c:if test="${ !authenticated }">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/login"/>">Se connecter <i
                                class="fas fa-sign-in-alt"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/register"/>">S'inscrire</a>
                    </li>
                </c:if>
            </ul>
            <%--        <form class="form-inline my-2 my-lg-0">--%>
            <%--            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--%>
            <%--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--%>
            <%--        </form>--%>
        </div>
    </div>
</nav>

<%-- Message d'alerte en cas d'erreur --%>
<c:if test="${ error != null }">
<div class="alert alert-danger alert-dismissible fade show" role="alert">
        ${ error }
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
</c:if>

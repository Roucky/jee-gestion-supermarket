<%--
  Created by IntelliJ IDEA.
  User: coren
  Date: 26/02/2020
  Time: 19:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="partials/header.jsp" %>
<h1 class="title">Mon panier</h1>
<div class="container">
    <form method="post">
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="inputCodeBarre" placeholder="Entrez le code barre"
                   aria-label="Code barre" aria-describedby="button-buy" required autofocus>
            <div class="input-group-append">
                <button class="btn btn-outline-custom" name="action" value="plus" type="submit" id="button-buy">Ajouter
                    au panier
                </button>
            </div>
        </div>
    </form>
</div>
<c:choose>
    <c:when test="${ cart.size() > 0 }">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Code barre</th>
                <th scope="col">Libellé</th>
                <th scope="col">Quantité</th>
                <th scope="col">Prix Unitaire (TTC)</th>
                <th scope="col0">Prix total (TTC)</th>
                <th scope="col0">TVA</th>
            </tr>
            </thead>
            <tbody>

                <%-- Déclaration de variable qui s'incrément à chaque ligne du panier, afin d'avoir les infos du total --%>

            <c:set value="0" var="totalPrixHT"/>
            <c:set value="0" var="totalTVA"/>
            <c:set value="0" var="totalPrixTTC"/>

            <c:forEach items="${ cart.keySet() }" var="codeBarre">
                <c:set value="${ articles.get(codeBarre) }" var="article"/>
                <c:set value="${ cart.get(codeBarre) }" var="quantite"/>

                <c:set value="${ totalPrixHT + article.prixHTEuro * quantite }" var="totalPrixHT"/>
                <c:set value="${ totalTVA + article.TVAEuro * quantite }" var="totalTVA"/>
                <c:set value="${ totalPrixTTC + article.prixTTCEuro * quantite }" var="totalPrixTTC"/>
                <tr>
                    <td><img class="card-img-top-cart" src="${ article.urlImg }"/></td>
                    <th scope="row">${ article.codeBarre }</th>
                    <td>${ article.libelle }</td>
                    <td>
                        <form action="cart" method="post">
                            <input type="hidden" name="inputCodeBarre" value="${ codeBarre }">
                            <div class="input-group mx-auto">
                                <div class="input-group-prepend">
                                    <button type="submit" name="action" value="minus" class="btn btn-outline-secondary">
                                        -
                                    </button>
                                </div>
                                <input type="text" class="inputQuantity" value="${ quantite }" readonly="readonly">
                                <div class="input-group-append">
                                    <button type="submit" name="action" value="plus" class="btn btn-outline-secondary">
                                        +
                                    </button>
                                </div>
                            </div>
                        </form>
                    </td>
                    <td><fmt:formatNumber value="${ article.prixTTCEuro }" pattern="0.00" type="currency"/> €</td>
                    <td><fmt:formatNumber value="${ article.prixTTCEuro * cart.get(codeBarre) }" pattern="0.00"
                                          type="currency"/> €
                    </td>
                    <td><fmt:formatNumber value="${ article.TVAEuro * cart.get(codeBarre) }" pattern="0.00"
                                          type="currency"/> €
                    </td>
                </tr>
            </c:forEach>

            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <b>Total HT : </b><fmt:formatNumber value="${ totalPrixHT }" pattern="0.00" type="currency"/> €<br>
                    <b>Total TVA : </b><fmt:formatNumber value="${ totalTVA }" pattern="0.00" type="currency"/> €


                </td>
                <td></td>
            </tr>
            <tr>
                <th><u>Total</u></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <b>Total TTC : </b><fmt:formatNumber value="${ totalPrixTTC }" pattern="0.00" type="currency"/> €
                </td>
                <td></td>
            </tr>
            </tfoot>
        </table>
    </c:when>
    <c:otherwise>
        <h1 class="title">Votre panier est vide !</h1>
    </c:otherwise>
</c:choose>


<%@ include file="partials/footer.jsp" %>